﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using BooksMySqlMvc.Model;
using BooksMySqlMvc.Model.Entities;
using BooksMySqlMvc.Views;
using System.Windows.Forms;

namespace BooksMySqlMvc.Controllers
{
    class ControllerForMain
    {
        private FormMain form;
        private DbManager db;
        private bool sortAsc;


        public ControllerForMain(FormMain form)
        {
            this.form = form;
            sortAsc = false;
            
            try
            {
                db = DbManager.GetDbManager();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public void UpdateDateGridViewBooks()
        {
            form.dataGridViewBooks.DataSource = null;
            
            try
            {
                ReopenConnectionIfCrash();

                form.dataGridViewBooks.DataSource = db.TableBooks.GetAllBooks();

                form.dataGridViewBooks.Columns["ArticleCd"].Visible = false;
                form.dataGridViewBooks.Columns["Title"].HeaderText = "Название";
                form.dataGridViewBooks.Columns["Author"].HeaderText = "Автор";
                form.dataGridViewBooks.Columns["PublicationDate"].HeaderText = "Дата публикации";
                form.dataGridViewBooks.Columns["DateOfReceipt"].HeaderText = "Дата поступления";

                form.dataGridViewBooks.ClearSelection();
            }
            catch
            {
                MessageBox.Show("Нет соединения с базой данных\nПриложение будет закрыто!");

                Application.Exit();
            }
        }

        public void DeleteBook()
        {
            if (form.dataGridViewBooks.SelectedRows.Count == 0)
            {
                MessageBox.Show("Нет выбранных строк!");
                return;
            }

            int articleCd = (int)form.dataGridViewBooks.SelectedRows[0].Cells["ArticleCd"].Value;
                        
            try
            {
                ReopenConnectionIfCrash();

                db.TableBooks.DeleteBookByArticleCd(articleCd);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public void AddBook()
        {
            if (CheckTheFillingOfFields())
            {
                Book book = new Book()
                {
                    Title = form.textBoxTitle.Text,
                    Author = form.textBoxAuthor.Text,
                    PublicationDate = Convert.ToInt32(form.textBoxPublicationDate.Text),
                    DateOfReceipt = form.dateTimePickerDateOfReceipt.Value.Date
                };

                try
                {
                    ReopenConnectionIfCrash();

                    db.TableBooks.AddNewBook(book);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }
        }

        public void UpdateBook()
        {
            if (form.dataGridViewBooks.SelectedRows.Count == 0)
            {
                MessageBox.Show("Выберите обновляемую строку");
                return;
            }

            if (CheckTheFillingOfFields())
            {
                int articleCd = (int)form.dataGridViewBooks.SelectedRows[0].Cells["ArticleCd"].Value;

                Book book = new Book()
                {
                    ArticleCd = articleCd,
                    Title = form.textBoxTitle.Text,
                    Author = form.textBoxAuthor.Text,
                    PublicationDate = Convert.ToInt32(form.textBoxPublicationDate.Text),
                    DateOfReceipt = form.dateTimePickerDateOfReceipt.Value.Date
                };

                try
                {
                    ReopenConnectionIfCrash();

                    db.TableBooks.UpdateBookByArticleCd(book);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }
        }

        public void SortColumn(int columnIndex)
        {
            List<Book> books = form.dataGridViewBooks.DataSource as List<Book>;
            List<Book> sortedBooks = null;

            switch (columnIndex)
            {
                case 1:
                    sortedBooks = sortAsc == true ? books.OrderBy(item => item.Title).ToList() : books.OrderByDescending(item => item.Title).ToList();
                    break;
                case 2:
                    sortedBooks = sortAsc == true ? books.OrderBy(item => item.Author).ToList() : books.OrderByDescending(item => item.Author).ToList();
                    break;
                case 3:
                    sortedBooks = sortAsc == true ? books.OrderBy(item => item.PublicationDate).ToList() : books.OrderByDescending(item => item.PublicationDate).ToList();
                    break;
                case 4:
                    sortedBooks = sortAsc == true ? books.OrderBy(item => item.DateOfReceipt).ToList() : books.OrderByDescending(item => item.DateOfReceipt).ToList();
                    break;
            }

            sortAsc = !sortAsc;

            form.dataGridViewBooks.DataSource = null;
            form.dataGridViewBooks.DataSource = sortedBooks;

            form.dataGridViewBooks.Columns["ArticleCd"].Visible = false;
            form.dataGridViewBooks.Columns["Title"].HeaderText = "Название";
            form.dataGridViewBooks.Columns["Author"].HeaderText = "Автор";
            form.dataGridViewBooks.Columns["PublicationDate"].HeaderText = "Дата публикации";
            form.dataGridViewBooks.Columns["DateOfReceipt"].HeaderText = "Дата поступления";

            form.dataGridViewBooks.ClearSelection();
        }

        public void ClearFields()
        {
            form.textBoxTitle.Clear();
            form.textBoxAuthor.Clear();
            form.textBoxPublicationDate.Clear();
            form.dateTimePickerDateOfReceipt.Value = DateTime.Now;
        }

        public void FillField()
        {
            if (form.dataGridViewBooks.SelectedRows.Count != 0)
            {
                Book selectedBook = form.dataGridViewBooks.SelectedRows[0].DataBoundItem as Book;

                form.textBoxTitle.Text = selectedBook.Title;
                form.textBoxAuthor.Text = selectedBook.Author;
                form.textBoxPublicationDate.Text = selectedBook.PublicationDate.ToString();
                form.dateTimePickerDateOfReceipt.Value = selectedBook.DateOfReceipt;
            }
        }

        private bool CheckTheFillingOfFields()
        {
            if (form.textBoxTitle.TextLength == 0)
            {
                MessageBox.Show("Не заполнено поле - Название");
                return false;
            }
            else if (form.textBoxAuthor.TextLength == 0)
            {
                MessageBox.Show("Не заполнено поле - Автор");
                return false;
            }
            else if (form.textBoxPublicationDate.TextLength == 0)
            {
                MessageBox.Show("Не заполнено поле - Дата публикации");
                return false;
            }

            return true;
        }

        private void ReopenConnectionIfCrash()
        {
            if (db.StateSqlConnect == ConnectionState.Closed)
            {
                db.Reopen();
            }
        }


    }
}

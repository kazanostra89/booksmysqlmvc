﻿using System;
using System.Windows.Forms;
using BooksMySqlMvc.Controllers;

namespace BooksMySqlMvc.Views
{
    public partial class FormMain : Form
    {
        private ControllerForMain controller;


        public FormMain()
        {
            InitializeComponent();
            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            controller = new ControllerForMain(this);
        }

        private void textBoxTitle_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Space)
            {
                if (textBoxTitle.TextLength == 0 || textBoxTitle.Text.EndsWith(" "))
                {
                    e.Handled = true;
                }
            }
        }

        private void textBoxAuthor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= (char)1040 && e.KeyChar <= (char)1103) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space)
            {
                if (e.KeyChar == (char)Keys.Space)
                {
                    if (textBoxAuthor.TextLength == 0 || textBoxAuthor.Text.EndsWith(" "))
                    {
                        e.Handled = true;
                    }
                }

                return;
            }

            e.Handled = true;
        }

        private void textBox_Leave(object sender, EventArgs e)
        {
            if (sender is TextBox && ((TextBox)sender).TextLength >= 2)
            {
                TextBox textBox = sender as TextBox;

                if (textBox.Name != "textBoxPublicationDate" && (textBox.Text.StartsWith(" ") || textBox.Text.EndsWith(" ")))
                {
                    textBox.Text = textBox.Text.Trim(' ');
                    return;
                }

                if (textBox.Name == "textBoxPublicationDate" && textBox.Text.StartsWith("0"))
                {
                    textBox.Text = textBox.Text.TrimStart('0');
                }
            }
        }

        private void textBoxPublicationDate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= (char)48 && e.KeyChar <= (char)57 && textBoxPublicationDate.TextLength < 4)
                || e.KeyChar == (char)Keys.Back)
            {
                if (e.KeyChar == (char)Keys.D0 && textBoxPublicationDate.TextLength == 0)
                {
                    e.Handled = true;
                }

                return;
            }

            e.Handled = true;
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            controller.UpdateDateGridViewBooks();
        }

        private void buttonInsert_Click(object sender, EventArgs e)
        {
            controller.AddBook();

            controller.UpdateDateGridViewBooks();
        }

        private void buttonUpdateData_Click(object sender, EventArgs e)
        {
            controller.UpdateDateGridViewBooks();

            controller.ClearFields();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            controller.DeleteBook();

            controller.UpdateDateGridViewBooks();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            controller.UpdateBook();

            controller.ClearFields();

            controller.UpdateDateGridViewBooks();
        }

        private void dataGridViewBooks_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            controller.FillField();
        }

        private void dataGridViewBooks_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            controller.SortColumn(e.ColumnIndex);

            controller.ClearFields();
        }
    }
}

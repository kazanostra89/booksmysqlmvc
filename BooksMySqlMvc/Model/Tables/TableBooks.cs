﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using BooksMySqlMvc.Model.Entities;

namespace BooksMySqlMvc.Model.Tables
{
    class TableBooks
    {
        private MySqlCommand cmdSql;


        public TableBooks(MySqlCommand cmdSql)
        {
            this.cmdSql = cmdSql;
        }

        public List<Book> GetAllBooks()
        {
            List<Book> books = new List<Book>();

            cmdSql.CommandText = "CALL books_select_all();";

            MySqlDataReader reader = cmdSql.ExecuteReader();

            while (reader.Read())
            {
                books.Add(new Book()
                {
                    ArticleCd = reader.GetInt32("ArticleCD"),
                    Title = reader.GetString("Title"),
                    Author = reader.GetString("Author"),
                    PublicationDate = reader.GetInt32("PublicationDate"),
                    DateOfReceipt = reader.GetDateTime("DateOfReceipt").Date
                });
            }

            reader.Close();

            return books;
        }

        public void DeleteBookByArticleCd(int articleCd)
        {
            cmdSql.CommandText = $"CALL books_delete_by_articleCD(@articleCd);";

            cmdSql.Parameters.AddWithValue("@articleCd", articleCd);

            cmdSql.ExecuteNonQuery();

            cmdSql.Parameters.Clear();
        }

        public void AddNewBook(Book book)
        {
            cmdSql.CommandText = "CALL books_insert_new(@title, @author, @publicationDate, @dateOfReceipt);";

            cmdSql.Parameters.AddWithValue("@title", book.Title);
            cmdSql.Parameters.AddWithValue("@author", book.Author);
            cmdSql.Parameters.AddWithValue("@publicationDate", book.PublicationDate);
            cmdSql.Parameters.AddWithValue("@dateOfReceipt", book.DateOfReceipt);

            cmdSql.ExecuteNonQuery();

            cmdSql.Parameters.Clear();
        }

        public void UpdateBookByArticleCd(Book book)
        {
            cmdSql.CommandText = "CALL books_update_by_articleCD(@articleCd, @title, @author, @publicationDate, @dateOfReceipt);";

            cmdSql.Parameters.AddWithValue("@articleCd", book.ArticleCd);
            cmdSql.Parameters.AddWithValue("@title", book.Title);
            cmdSql.Parameters.AddWithValue("@author", book.Author);
            cmdSql.Parameters.AddWithValue("@publicationDate", book.PublicationDate);
            cmdSql.Parameters.AddWithValue("@dateOfReceipt", book.DateOfReceipt);

            cmdSql.ExecuteNonQuery();

            cmdSql.Parameters.Clear();
        }
    }
}

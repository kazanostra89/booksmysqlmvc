﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using BooksMySqlMvc.Model.Tables;
using BooksMySqlMvc.Model.Tools;

namespace BooksMySqlMvc.Model
{
    class DbManager
    {
        private static DbManager manager = null;

        private MySqlConnection connectSql;
        private MySqlCommand cmdSql;
        private TableBooks tableBooks;


        private DbManager()
        {
            connectSql = ConnectionManager.GetMySqlConnection();

            cmdSql = connectSql.CreateCommand();

            tableBooks = new TableBooks(cmdSql);

            connectSql.Open();
        }

        ~DbManager()
        {
            connectSql.Close();
        }

        public static DbManager GetDbManager()
        {
            if (manager == null)
            {
                manager = new DbManager();
            }

            return manager;
        }

        public void Reopen()
        {
            connectSql.Open();
        }


        public TableBooks TableBooks
        {
            get { return tableBooks; }
        }

        public ConnectionState StateSqlConnect
        {
            get { return connectSql.State; }
        }

    }
}

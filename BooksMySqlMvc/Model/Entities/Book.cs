﻿using System;

namespace BooksMySqlMvc.Model.Entities
{
    class Book
    {
        public int ArticleCd { set; get; }
        public string Title { set; get; }
        public string Author { set; get; }
        public int PublicationDate { set; get; }
        public DateTime DateOfReceipt { set; get; }
    }
}

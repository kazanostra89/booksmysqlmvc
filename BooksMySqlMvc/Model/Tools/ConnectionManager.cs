﻿using System;
using MySql.Data.MySqlClient;

namespace BooksMySqlMvc.Model.Tools
{
    class ConnectionManager
    {
        public static MySqlConnection GetMySqlConnection()
        {
            string connectionString = "Server=127.0.0.1;Port=3306;User=root;Password=1234;Database=library;";

            return new MySqlConnection(connectionString);
        }
    }
}
